# Project Quizz

Ce projet a consisté à réaliser un quiz sur le thème de notre choix. Celui-ci est autour de la musique des années 80.
Il comporte 4 questions avec 4 réponses au choix. Si la bonne réponse est choisie, un message indique que c'est la bonne réponse et un point est ajouté au score. Si une mauvaise réponse est choisie, un message indique la bonne réponse. 
Un essai maximum par question et un point par question.

Méthodologie chronologique de la rédaction du TypeScript:
- Création des tableaux des questions et réponses.
- Création des variables correspondant au chaque élément HTML (div et boutons).
- Création de la fonction permettant l'affichage des questions et des réponses et son évênement correspondant (au "click" sur Next).
- Création de la fonction permettant de comparer la réponse sélectionnée et la bonne réponse.
- Création de la fonction recommencer et son évement correspondant (au "click" sur restart).
- Création de la fonction d'affichage du compteur des questions.
- Création de la fonction d'affichage du score.
- Création de la fonction de reinitialisaiton du score.

- Création de la fonction de désactivation des réponses.
- Création de la fonction de réactivation des réponses.


Le style à été fait en aval du JS, après avoir vérifié la fonctionnalité du jeu.
Lien Figma (maquettes): https://www.figma.com/file/0WEy0fU15kJW9Kzet8k2Sn/Quiz-project?node-id=0%3A1&t=sF65jg8P5aVxTbcn-0
Lien Netlify : https://projet-quiz.netlify.app/




