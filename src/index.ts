// D TABLEAUX //
let arrayQuestions:string[] =  ['Qui est le chanteur de la chanson "I want to break free"?',
                                'Qui chantait "Partenaires particuliers"?',
                                'De quel groupe très connu George Michael etait-il le chanteur?',
                                'Quel groupe interprétait "Viens boire un petit coup à la maison?"'];

let arrayAnswers:string[] []=   [[  'Prince', 'David Bowie', 'Freddy Mercury', 'Lionel Ritchie'], 
                                [   'Emile et Image', 'Mylène Farmer', 'Partenaires Particuliers', 'David et Jonathan'],
                                [   'A-Ha !', 'Talk Talk', 'Wham!', 'Bananarama'],
                                [   'Force 4', 'Puissance 4', 'Licence 4', 'Les 4 Fantastiques']];
                                
                                
let arrayCorrectAnswer:string[] = ['Freddy Mercury', 'Partenaires Particuliers', 'Wham!', 'Licence 4'];
// F TABLEAUX //


// D VARIABLES //
const question =document.querySelector<HTMLElement>('.question'); /*champ des questions*/
const select =document.querySelector<HTMLElement>('.middle'); /*champ des reponses*/
const reponseA = document.querySelector<HTMLElement>('.reponseA');
const reponseB = document.querySelector<HTMLElement>('.reponseB');
const reponseC = document.querySelector<HTMLElement>('.reponseC');
const reponseD = document.querySelector<HTMLElement>('.reponseD');
const solution = document.querySelector<HTMLElement>('.solution');
const points = document.querySelector<HTMLElement>('.points');
const compteur = document.querySelector<HTMLElement>('.compteur');

const restart = document.querySelector<HTMLButtonElement>('.restart');
const next = document.querySelector<HTMLButtonElement>('.next');
const commencer = document.querySelector<HTMLButtonElement>('.commencer');
const intro = document.querySelector<HTMLButtonElement>('.intro');
// F VARIABLES //



// D STATUT DE DEPART DU JEU //
/**
 * Les compteurs sont à zéro.
 * Les fonctions sont dans leur état initial.
 */
let counterPoints = 0;
let counterQuestion=0;
let counterAnswer=0;

reEnable();
unClickMax();
nextQuestion();
nextAnswers();
recommencer();
compteurQuestion();
start();
// D STATUT DE DEPART DU JEU //



// D ASSIGNATION DE LA REPONSE  //
    if(reponseA){
        reponseA.textContent = arrayAnswers[0] [0];
    }
    if(reponseB){
        reponseB.textContent = arrayAnswers[0] [1];
    }
    if(reponseC){
        reponseC.textContent = arrayAnswers[0] [2];
    }
    if(reponseD){
        reponseD.textContent = arrayAnswers[0] [3];
    }
    if(next){
        next.innerHTML = 'NEXT';
    }
// F ASSIGNATION DE LA REPONSE  //


// FONCTION START //
/**
 * Fonction qui cache la section Intro au clic sur bouton commencer.
 * @param click - lorsque l'évênement est cliqué.
 * @param pointerEvents - le bouton n'est plus un pointeur.
 */
function start(){
    commencer?.addEventListener('click', ()=>{
        if(intro){
            intro.style.opacity="0";
            intro.style.pointerEvents = "none"; 
        }
        
    })
} 
// FONCTION START //


// D ASSIGNATION DE LA QUESTION  // 
/**
 *Fonction pour afficher les questions.
 @param arrayQuestions - tableau des questions.
 @param counterQuestions - compteur parcourant le tableau des questions.
 * 
 */
function nextQuestion(){
    if(question){
        question.innerHTML = arrayQuestions[counterQuestion];
    }
}
// F ASSIGNATION DE LA QUESTION  //


// D ASSIGNATION DE LA REPONSE  // 
/**
 * Fonction pour afficher une nouvelle reponse au click sur Next.
 * @param arrayAnswers - tableau des réponses.
 * @param counterAnswers - compteur parcourant le tableau des réponses.
 */
function nextAnswers(){
        if(reponseA){
            reponseA.innerHTML = arrayAnswers[counterAnswer][0];
        }
        if(reponseB){
            reponseB.innerHTML = arrayAnswers[counterAnswer][1];
        }
        if(reponseC){
            reponseC.innerHTML = arrayAnswers[counterAnswer][2];
        }
        if(reponseD){
            reponseD.innerHTML = arrayAnswers[counterAnswer][3];
        }
}
// F ASSIGNATION DE LA REPONSE  // 


/* D EVENT AU CLICK SUR NEXT*/

/**
 * Evênement permettant d'incrémenter le compteur des questions et réponses,
 * afficher le html du bouton next et de la solution.
 */
next?.addEventListener('click', () => {
    counterAnswer++;
    counterQuestion++;
    
    compteurQuestion();
    nextAnswers();
    nextQuestion();
    recommencer();
    reEnable();
    
    if(solution){
        solution.innerHTML = '...';
    }
    if(next){
        next.innerHTML = 'NEXT';
    }
});
// F EVENT AU CLICK SUR NEXT//


// D CHOIX DE LA REPONSE//

let choixDeReponse= '';

/**
 * Si une des réponses est cliquée, elle compare le le choix fait et la reponse du tableau de réponses.
 * @param choixDeReponse - variable correspondant à la réponse choisie.
 */
if(select){
    reponseA?.addEventListener('click', ()=> {
    choixDeReponse = arrayAnswers[counterAnswer][0];
    comparer();
    
    })
    reponseB?.addEventListener('click', ()=> {
    choixDeReponse = arrayAnswers[counterAnswer][1];
    comparer();
    
    })
    reponseC?.addEventListener('click', ()=> {
    choixDeReponse = arrayAnswers[counterAnswer][2];
    comparer();
    
    })
    reponseD?.addEventListener('click', ()=> {
    choixDeReponse = arrayAnswers[counterAnswer][3];
    comparer();
    
    })

}
/**
 * Affiche le score sur 4.
 * @param counterPoints - compteur des points.Number converti en string.
 */
if(points){
    points.textContent = 'Score: '+ String(Number(counterPoints)) + ' /4';;
}
if(solution){
    solution.textContent = '...';
}

// D CHOIX DE LA REPONSE
/**
 * Fonction pour comparer la reponse choisie et la réponse correcte et afficher un message.
 * Si la comparaison est true, incrémentation  du compteur de points et affichage du score
 * après incrémentation des point. Et affichage du message Bonne reponse.
 * @param choixDeReponse - corresponds à la rponse choisie.
 * @param arrayCorrectAnswer -  tableau des bonnes réponses.
 * @param counterAnswer - compteur des réponses.
 */
function comparer (){
    if (choixDeReponse== arrayCorrectAnswer[counterAnswer]){
        console.log('vrai'); 
        if(points){
            counterPoints++;
            points.textContent = 'Score: ' + String(Number(counterPoints)) + ' /4'; /*assignation du score*/  
        }
        if(solution){
            solution.innerHTML = 'Bonne réponse !';
        }
    }
    if(choixDeReponse != arrayCorrectAnswer[counterAnswer]){
        console.log('faux');
        if(solution){
            solution.innerHTML = 'La bonne réponse était ... ' + arrayCorrectAnswer[counterAnswer];
        }    
    }
}
// F CHOIX DE LA REPONSE


// D RESTART //
/**
 * Fonction pour cacher le bouton Restart et afficher le bouton Next
 * lorsque la 4eme question est affichée.
 */
function recommencer(){
    if(counterQuestion < 3){
        if(restart){
            restart.style.display = 'none';
        }
        if(next){
            next.style.display = 'block';
        }
        }else{
            if(restart){
                restart.style.display = 'block';
            }
            if(next){
                next.style.display = 'none';
            }
        }
}


/**
 * Evenement reinitialisant le jeu au clic sur restart.
 * @param nextQuestions - renvoie a la 1ere question.
 * @param nextAnswers - renvoie a la 1ere reponse.
 * @param recommencer - affiche le bouton Next.
 * @param compteurQuestion - reprends le comptage du tableau de questions.
 * @param resetPoints - remets les points a zéro.
 * 
 */
restart?.addEventListener('click', ()=>{
    counterQuestion=0;
    counterAnswer=0;
    
    nextQuestion();
    nextAnswers();
    recommencer();
    compteurQuestion();
    resetPoints();
    
    if(solution){
        solution.innerHTML = '';
    } 
})
// F RESTART //



// D COMPTEUR DE QUESTION //
/**
 * Fonction d'incrémentation des points pour afficher le score.
 */
function compteurQuestion(){
    if(compteur){
        compteur.innerHTML = 'Question ' + String(Number(counterQuestion)+1) + " /4";
    }
}
// F COMPTEUR DE QUESTION //




// D RESET POINTS //
/**
 * Fonction reinitialisant les points a zéro.
 * @param counterPoints - compteur des points.
 */
function resetPoints(){
    if(points){
        counterPoints=0;
        points.textContent = String(Number(counterPoints)) + ' /4'; /*assignation du score*/
    }
}
// D RESET POINTS //




// D UN CLICK MAX PAR QUESTION//
/**
 * Fonction pour désactiver les reponses.
 * @param opacity - opacité
 * @param pointerEvents - comprends la div comme un pointeur.
 */
function unClickMax(){
    select?.addEventListener('click', ()=>{
        if(select){
            select.style.opacity="0.5";
            select.style.pointerEvents = "none";
        }
    })
}
/**
 * Fonction réactiver les réponses.
 */
function reEnable(){
        if(select){
            select.style.opacity="1";
            select.style.pointerEvents = "auto";
        }
}
// F UN CLICK MAX PAR QUESTION//










       














    

    









       















































